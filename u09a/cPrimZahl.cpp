#include "cPrimZahl.h"

cPrimZahl::cPrimZahl(int start_in) {
	startzahl = start_in;

	if (startzahl > maxprim) startzahl = maxprim;
	else if (startzahl < 1) startzahl = 1;

	aktprim = nextPrime(startzahl);
}

// Friend methods
// overwrite output stream
ostream& operator << (ostream& os, const cPrimZahl& obj) {
	os << obj.aktprim;
	return os;
}

// overwrite prefix increment
cPrimZahl& operator ++ (cPrimZahl& obj) {
	int tmpIncr = obj.aktprim + 1;
	obj.aktprim = obj.nextPrime(tmpIncr);

	return obj;
}

// overwrite prefix decrement
cPrimZahl& operator -- (cPrimZahl& obj) {
	int tmpIncr = obj.aktprim - 1;
	obj.aktprim = obj.prevPrime(tmpIncr);

	return obj;
}

// overwrite subscript operator
// finds next prime number from index "val"
int& cPrimZahl::operator [] (const int& val) {
	if (val <= 0) return aktprim;

	static int cpyVal = val;
	if (isPrime(val)) return cpyVal;

	int tmpVal = cpyVal + 1;
	static int prim = nextPrime(tmpVal);

	return prim;
}

// check if number is a prime
bool cPrimZahl::isPrime(int n) { // Pruefung einer Zahl auf Primzahl
	for (int i = 2; i <= n / 2; i++) {
		if (n % i) {
			continue;
		}
		else {
			return false;
		}
	}
	return true;
}

// find next prime number from start-value "start"
int cPrimZahl::nextPrime(int& start) {
	int prim = 0;

	// find prime number between start and maxprim
	for (int i = start; i < maxprim; i++) {
		if (isPrime(i)) {
			prim = i;
			break;
		}
	}

	// find highest prime number under maxprim
	// if first loop couldnt find a prime number
	if (prim == 0) {
		for (int j = maxprim; j > 2; j--) {
			if (isPrime(j)) {
				prim = j;
				break;
			}
		}
	}

	return prim;
}

// find prime number previous from start
int cPrimZahl::prevPrime(int& start) {
	int prim = 2;

	for (int j = start; j >= 2; j--) {
		if (isPrime(j)) {
			prim = j;
			break;
		}
	}

	return prim;
}