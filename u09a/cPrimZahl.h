#pragma once

#include <iostream>

using namespace std;
const int maxprim = 10000;

class cPrimZahl
{
private:
	int aktprim;
	int startzahl;
	bool isPrime(int);
	int nextPrime(int&);
	int prevPrime(int&);
	friend ostream& operator << (ostream&, const cPrimZahl&);
	friend cPrimZahl& operator ++ (cPrimZahl&);
	friend cPrimZahl& operator -- (cPrimZahl&);
public:
	cPrimZahl(int = 1);
	int& operator [] (const int&);
};

