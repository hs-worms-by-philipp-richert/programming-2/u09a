#include "cPrimZahl.h"
#include <string>

int main() {
	cPrimZahl pz = cPrimZahl(20);
	string input;
	int zahlInput;
	int i;

	cout << "Aktuelle Primzahl: " << pz << endl;

	do {
		cout << "Befehl eingeben (+, -, s, e(xit)): ";
		cin >> input;

		if (input == "+") 
			++pz;
		else if (input == "-")
			--pz;
		else if (input == "s") {
			cout << "Zahl eingeben: ";
			cin >> zahlInput;

			i = pz[zahlInput];
			cout << "Naechste Primzahl von " << zahlInput << " ist " << i << endl;
		}

		cout << "Aktuelle Primzahl: " << pz << endl;
	} while (input != "e");

	return 0;
}